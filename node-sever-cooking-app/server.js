const express = require('express');
const bodyParser = require('body-parser');
const db = require('./config/db.config');
const app = express();

const port = process.env.PORT || 5000;

app.use(bodyParser.urlencoded({ extended: true }))

app.use(bodyParser.json())

app.post('/register', (req, res) => {
  const { email, password, phone, fullname } = req.body;
  db.query('INSERT INTO user (email, password, phone, fullname) VALUES (?, ?, ?, ?)', [email, password, phone, fullname], (err, results) => {
    if (err) {
      res.status(500).json({ error: 'Registration failed' });
    } else {
      res.json({ message: 'Registration successful' });
    }
  });
})
app.post('/login', (req, res) => {
  const { email, password } = req.body;
  db.query('SELECT * FROM user WHERE email = ? AND password = ?', [email, password], (err, results) => {
    if (err) {
      res.status(500).json({ error: 'Login failed' });
    } else if (results.length === 1) {
      res.json({ message: 'Login successful' });
    } else {
      res.status(401).json({ error: 'Invalid email or password' });
    }
  });
});
app.get('/recipes', (req, res) => {
  // Perform a SQL JOIN operation to retrieve data from both Recipe and User tables
  db.query('SELECT Recipe.id, Recipe.title, Recipe.cookingMinutes, Recipe.sourceUrl,Recipe.instructions, User.fullname ' +
           'FROM Recipe ' +
           'INNER JOIN User ON Recipe.userid = User.id', (err, results) => {
    if (err) {
      res.status(500).json({ error: 'Failed to retrieve recipes' });
    } else {
      res.json(results);
    }
  });
});
app.get('/recipe/:id', (req, res) => {
  const recipeId = req.params.id;

  // Perform a SQL JOIN operation to retrieve data from Recipe, User, and Ingredient tables for the given recipe ID
  db.query(
    'SELECT Recipe.id, Recipe.title, Recipe.cookingMinutes, Recipe.sourceUrl, User.fullname, Ingredient.name, Ingredient.amount, Ingredient.unit ' +
      'FROM Recipe ' +
      'INNER JOIN User ON Recipe.userid = User.id ' +
      'LEFT JOIN Ingredient ON Recipe.id = Ingredient.recipe_id ' +
      'WHERE Recipe.id = ?',
    [recipeId],
    (err, results) => {
      if (err) {
        res.status(500).json({ error: 'Failed to retrieve recipe' });
      } else {
        const recipeData = results.map((row) => ({
          id: row.id,
          title: row.title,
          cookingMinutes: row.cookingMinutes,
          sourceUrl: row.sourceUrl,
          fullname: row.fullname,
          ingredients: results
            .filter((ingredient) => ingredient.id === row.id)
            .map((ingredient) => ({
              name: ingredient.name,
              amount: ingredient.amount,
              unit: ingredient.unit,
            })),
        }))[0]; // Assuming there's only one recipe with the given ID

        res.json(recipeData);
      }
    }
  );
});
app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
