'use strict';
const mysql = require('mysql2');

const db = mysql.createPool({
  connectionLimit: 10, 
  host: '127.0.0.1',
  port: 3306, 
  user: 'root',
  password: '123456',
  database: 'cookingapp'
});

db.getConnection(function(err, connection) {
  if (err) {
    console.error('Error connecting to MySQL:', err);
    throw err;
  }
  console.log('Database Connected!');
  connection.release();
});

module.exports = db;
