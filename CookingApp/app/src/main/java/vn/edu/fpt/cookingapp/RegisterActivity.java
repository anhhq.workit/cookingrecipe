package vn.edu.fpt.cookingapp;

import static vn.edu.fpt.cookingapp.API.API_COOKING;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import vn.edu.fpt.cookingapp.model.User;
import vn.edu.fpt.cookingapp.model.UserResponse;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {
    private SharedPreferences sharedpreferences;
    private EditText txtFullName, txtEmail, txtPassword, txtPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        txtFullName = findViewById(R.id.txt_name);
        txtEmail = findViewById(R.id.txt_email);
        txtPassword = findViewById(R.id.txt_password);
        txtPhone = findViewById(R.id.txt_phone);
    }

    private void performRegister(String userJson) {
        String registerUrl = API_COOKING + "register/";

        JSONObject jsonRequest;
        try {
            jsonRequest = new JSONObject(userJson);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, registerUrl, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        handleRegisterResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleError(error);
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }

    private void handleRegisterResponse(JSONObject response) {
        Gson gson = new Gson();
        UserResponse registerResponse = gson.fromJson(response.toString(), UserResponse.class);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        if (registerResponse != null && registerResponse.getMessage().equals("Registration successful")) {
            startActivity(new Intent(this, LoginActivity.class));
        } else {
            Toast.makeText(this, "Registration failed. Please check your credentials.", Toast.LENGTH_LONG).show();
        }
    }

    private void handleError(VolleyError error) {
        Toast.makeText(this, "Registration failed. Please try again later.", Toast.LENGTH_LONG).show();
    }

    public void login(final View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void register(final View view) {
        String fullName = txtFullName.getText().toString().trim();
        String phone = txtPhone.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();

        // Validate input
        if (!isValidEmail(email)) {
            Toast.makeText(this, "Invalid email address.", Toast.LENGTH_LONG).show();
        } else if (!isValidPhone(phone)) {
            Toast.makeText(this, "Invalid phone number (must be 10 digits).", Toast.LENGTH_LONG).show();
        } else if (!isValidPassword(password)) {
            Toast.makeText(this, "Invalid password (must be at least 8 characters).", Toast.LENGTH_LONG).show();
        } else {
            User user = new User(email, password, phone, fullName);
            Gson gson = new Gson();
            String userJson = gson.toJson(user);
            performRegister(userJson);
        }
    }

    private boolean isValidEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        return Pattern.matches(emailPattern, email);
    }

    private boolean isValidPhone(String phone) {
        return phone.length() == 10 && phone.matches("\\d+");
    }

    private boolean isValidPassword(String password) {
        return password.length() >= 8;
    }
}
