package vn.edu.fpt.cookingapp;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class InstructionsFragment extends Fragment {
    private String title;
    private String urlImg;
    private String instructions;
    ImageView img;
    public InstructionsFragment() {
        // Required empty public constructor
    }

    public static InstructionsFragment newInstance(String title, String urlImg, String instructions) {
        InstructionsFragment fragment = new InstructionsFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("url_img", urlImg);
        args.putString("instructions", instructions);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString("title");
            urlImg = getArguments().getString("url_img");
            instructions = getArguments().getString("instructions");
        }
        img = getView().findViewById(R.id.sourceUrl_item);
        ImageView imageView = getView().findViewById(R.id.sourceUrl_item);
        TextView textView = getView().findViewById(R.id.txt_recipes);
        Glide.with(this)
                .load(urlImg)
                .apply(new RequestOptions().fitCenter())
                .into(img);
        textView.setText(instructions);
    }

}
