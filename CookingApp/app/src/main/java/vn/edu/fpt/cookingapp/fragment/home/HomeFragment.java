package vn.edu.fpt.cookingapp.fragment.home;

import static vn.edu.fpt.cookingapp.API.API_COOKING;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import vn.edu.fpt.cookingapp.Adapter.RecipeAdapter;
import vn.edu.fpt.cookingapp.R;
import vn.edu.fpt.cookingapp.model.Recipe;

public class HomeFragment extends Fragment {
    private RecyclerView recipeRecyclerView;
    private RecipeAdapter recipeAdapter;
    private List<Recipe> recipes = new ArrayList<>(); // Initialize the list here

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        recipeRecyclerView = view.findViewById(R.id.recipeRecyclerView);
        recipeRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        recipeAdapter = new RecipeAdapter(recipes, requireContext()); // Pass the list and context to the adapter
        recipeRecyclerView.setAdapter(recipeAdapter);

        fetchRecipesFromAPI();

        return view;
    }

    private void fetchRecipesFromAPI() {
        String recipesUrl = API_COOKING + "recipes";
        RequestQueue requestQueue = Volley.newRequestQueue(requireContext());
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, recipesUrl, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<Recipe> newRecipes = parseRecipesFromJson(response);
                        recipes.addAll(newRecipes); // Add the new recipes to the existing list
                        recipeAdapter.setRecipes(recipes); // Update the adapter
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(requireContext(), "Error fetching recipes: " + error, Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(request);
    }

    private List<Recipe> parseRecipesFromJson(JSONArray json) {
        List<Recipe> newRecipes = new ArrayList<>();

        try {
            for (int i = 0; i < json.length(); i++) {
                JSONObject jsonObject = json.getJSONObject(i);
                String id = jsonObject.getString("id");
                String title = jsonObject.getString("title");
                String cookingTime = jsonObject.getString("cookingMinutes");
                String url_img = jsonObject.getString("sourceUrl");
                String instruction = jsonObject.getString("instructions");
                Recipe recipe = new Recipe(Integer.parseInt(id), title, Integer.parseInt(cookingTime), url_img, instruction);
                newRecipes.add(recipe);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return newRecipes;
    }
}
