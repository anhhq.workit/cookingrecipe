package vn.edu.fpt.cookingapp.model;

public class User {
    private int id;
    private String email;
    private String password;
    private String phone;
    private String fullname;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(String email, String password, String phone, String fullname) {
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.fullname = fullname;
    }

    public User() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

}
