package vn.edu.fpt.cookingapp;

import static vn.edu.fpt.cookingapp.API.API_COOKING;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import vn.edu.fpt.cookingapp.model.User;
import vn.edu.fpt.cookingapp.model.UserResponse;

public class LoginActivity extends AppCompatActivity {

    private EditText txt_login, txt_password;
    private SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "Myapp" ;

    private static final String EMAIL = "emailKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        txt_login = findViewById(R.id.txt_login);
        txt_password = findViewById(R.id.txt_password);
    }

    public void btnLogin(final View view) {
        String email = txt_login.getText().toString().trim();
        String password = txt_password.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Email is Required.", Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Password is Required.", Toast.LENGTH_LONG).show();
            return;
        }

        if (password.length() < 6) {
            Toast.makeText(this, "Invalid password must > 6 character.", Toast.LENGTH_LONG).show();
            return;
        }

        User user = new User(email,password);
        Gson gson = new Gson();
        String userJson = gson.toJson(user);

        performLogin(userJson);
    }

    public void btnRegister(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    public void btnGuestLogin(View view) {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    private void performLogin(String userJson) {
        String loginUrl = API_COOKING + "login/";
        JSONObject jsonRequest;
        try {
            jsonRequest = new JSONObject(userJson);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, loginUrl, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        handleLoginResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleError(error);
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }

    private void handleLoginResponse(JSONObject response) {
        Gson gson = new Gson();
        UserResponse loginResponse = gson.fromJson(response.toString(), UserResponse.class);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        String email = txt_login.getText().toString().trim();

        if (loginResponse != null && loginResponse.getMessage().equals("Login successful")) {
            editor.putString(EMAIL, email);
            editor.commit();
            startActivity(new Intent(this, MainActivity.class));
        } else {
            Toast.makeText(this, "Login failed. Please check your credentials.", Toast.LENGTH_LONG).show();
        }
    }

    private void handleError(VolleyError error) {
        Toast.makeText(this, "Login failed. Please try again later.", Toast.LENGTH_LONG).show();
    }
}
