package vn.edu.fpt.cookingapp.model;

import androidx.lifecycle.ViewModel;

public class RecipeViewModel extends ViewModel {
    private String title;
    private String url_img;
    private String instructions;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrlImg() {
        return url_img;
    }

    public void setUrlImg(String urlImg) {
        this.url_img = urlImg;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
}
