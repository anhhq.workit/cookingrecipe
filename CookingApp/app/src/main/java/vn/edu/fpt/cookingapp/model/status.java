package vn.edu.fpt.cookingapp.model;

public class status {
    private boolean status ;

    public status(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
