package vn.edu.fpt.cookingapp;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;

public class RecipesActivity extends AppCompatActivity {

    ImageView img;
    TextView instruction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);
        setSupportActionBar(findViewById(R.id.toolbarRecipes));
        BottomNavigationView navView = findViewById(R.id.nav_view_recipes);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_Instruction, R.id.navigation_Ingredient)
                .build();
        NavController navCon = Navigation.findNavController(this, R.id.nav_host_fragment_recipes);
        NavigationUI.setupActionBarWithNavController(this, navCon, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navCon);


        String title = getIntent().getStringExtra("title");
        String urlImg = getIntent().getStringExtra("url_img");
        String instructions = getIntent().getStringExtra("instructions");

        InstructionsFragment instructionsFragment = InstructionsFragment.newInstance(title, urlImg, instructions);
        IngredientFragment ingredientFragment = IngredientFragment.newInstance(title, urlImg, instructions);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_recipes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            // Handle the "Save" menu item click
            // You can implement the save functionality here.
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
