package vn.edu.fpt.cookingapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.squareup.picasso.Picasso;

import java.util.List;

import vn.edu.fpt.cookingapp.R;
import vn.edu.fpt.cookingapp.RecipesActivity;
import vn.edu.fpt.cookingapp.model.Recipe;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder> {
    private List<Recipe> recipes;
    Context context;

    public RecipeAdapter(List<Recipe> recipes, Context context) {
        this.recipes = recipes;
        this.context = context;
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipe_item, parent, false);
        return new RecipeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder holder, int position) {
        Recipe recipe = recipes.get(position);

        holder.recipeTitle.setText(recipe.getTitle());
        holder.recipeCookingTime.setText("Cooking time: " + recipe.getCookingMinutes() + " minutes");
        Picasso.with(context).load(recipes.get(position).getSourceUrl()).into(holder.sourceUrl);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create an intent to open RecipeDetailActivity and pass data
                Intent intent = new Intent(context, RecipesActivity.class);
                intent.putExtra("title", recipe.getTitle());
                intent.putExtra("url_img", recipe.getSourceUrl());
                intent.putExtra("instructions", recipe.getInstructions());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
        notifyDataSetChanged();
    }

    static class RecipeViewHolder extends RecyclerView.ViewHolder {
        TextView recipeTitle;
        TextView recipeCookingTime;
        ImageView sourceUrl;

        RecipeViewHolder(View itemView) {
            super(itemView);
            recipeTitle = itemView.findViewById(R.id.titleRecipes);
            recipeCookingTime = itemView.findViewById(R.id.cookingMinutes);
            sourceUrl = itemView.findViewById(R.id.sourceUrl);
        }
    }
}
