package vn.edu.fpt.cookingapp.model;

import java.util.List;

public class Recipe {
    private int id;
    private String title;
    private String userid;
    private int cookingMinutes;
    private String sourceUrl;
    private List<Ingredient> ingredients;
    private String instructions;

    public Recipe(int id, String title, String userid, int cookingMinutes, String sourceUrl, List<Ingredient> ingredients, String instructions) {
        this.id = id;
        this.title = title;
        this.userid = userid;
        this.cookingMinutes = cookingMinutes;
        this.sourceUrl = sourceUrl;
        this.ingredients = ingredients;
        this.instructions = instructions;
    }

    public Recipe(int id, String title, String userid, int cookingMinutes) {
        this.id = id;
        this.title = title;
        this.userid = userid;
        this.cookingMinutes = cookingMinutes;
    }

    public Recipe(int id, String title, int cookingTime) {
        this.id = id;
        this.title = title;
        this.cookingMinutes = cookingTime;
    }

    public Recipe(int id, String title, int cookingTime, String urlImg) {
        this.id = id;
        this.title = title;
        this.cookingMinutes = cookingTime;
        this.sourceUrl = urlImg;
    }

    public Recipe(int id, String title, int cookingTime, String urlImg, String instructions) {
        this.id = id;
        this.title = title;
        this.cookingMinutes = cookingTime;
        this.sourceUrl = urlImg;
        this.instructions = instructions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getCookingMinutes() {
        return cookingMinutes;
    }

    public void setCookingMinutes(int cookingMinutes) {
        this.cookingMinutes = cookingMinutes;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
}
