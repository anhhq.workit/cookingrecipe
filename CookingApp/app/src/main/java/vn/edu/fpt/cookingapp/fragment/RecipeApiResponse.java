package vn.edu.fpt.cookingapp.fragment;

import java.util.List;

import vn.edu.fpt.cookingapp.model.Recipe;

public class RecipeApiResponse {
    private List<Recipe> recipes;

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }
}
